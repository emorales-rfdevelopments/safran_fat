# Safran LLRF FAT Test Scripts
Safran LLRF FAT Tango devices tests. This files are to test if hardware communication perform as expected and no problems are detected.

## Requirements
This scripts must run in a machine with access to Tango DB where LLRF device is instantiated.
YAML file with conda test environment provided.

## Conda environment
Conda enviroment provided in case machine were you will perform testsinside
_conda_env.yml_ file with:
* Python 3.10
* PyQt5
* Tango
* Taurus

### How to install conda
You need a laptop/pc with conda installed. If your system don't have conda installed,
please go [here for windows](https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html)
or [here for linux](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html).

### How to generate our test environment

To install from _conda_env.yml_ do:
```
conda env create -f conda_env.yml
```

## Scripts

### test_read.py
Script that reads whole Tango device attributes form a device server every 0.2 seconds 10000 times.
Using threading, we simmulate that 10 clients are doing this job at the same time.

**REMEMBER**: Change device name in line 52:
```
# Change Device name by your LLRF Tango device name:
dev_name = "safran/test/yaml"
```

### test_taurus.py
Script that launch 5 _taurus form_ with all attributes inside a device.

**REMEMBER**: Change device name in line 46:
```
# Change Device name by your LLRF Tango device name:
device = "sys/tg_test/1"
```

## About
* Author: E.Morales (emorales@cells.es)
* Copyright: Copyright 2024, CELLS / ALBA Synchrotron