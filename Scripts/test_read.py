# Test Read Script
# Copyright (C) 2024  Emilio Morales

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import time
import traceback
from threading import Thread

import tango

def ReadAll(device_proxy, sleep=0.2):

    _attrs = [a.name for a in device_proxy.attribute_list_query()]
    try:
        for attr in _attrs:
            print("Attr:{} - Value: {}".format(
                attr, device_proxy.read_attribute(attr).value)
            )
            time.sleep(sleep)
    except Exception as e:
        print("Problems in ReadAll method: {}".format(e))
        print(traceback.format_exc())

def main(dev_name):

    dp = tango.DeviceProxy(dev_name)

    for i in range(1000):
        print("Reading attributes from: {}".format(dev_name))
        ReadAll(dp)


if __name__ == "__main__":

    print("Starting...")
    start_time = time.time()
    myThreadList = []

    # Change Device name by your LLRF Tango device name:
    dev_name = "safran/test/yaml"

    # Create 10 Threads:
    for i in range(10):
        myThread = Thread(target=main, args={dev_name})
        myThreadList.append(myThread)

    # Start Threads:
    for item in myThreadList:
        item.start()

    # Wait until all Threads end:
    for item in myThreadList:
        item.join()

    end_time = time.time() - start_time

    print("Elapsed time: {}".format(end_time))

