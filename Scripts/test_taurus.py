# Test Taurus Script
# Copyright (C) 2024  Emilio Morales

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import time
from subprocess import Popen

from taurus import Device

def main(device: str, num_of_threads: int=5):
    """Method that will launch N independent taurus form processes with all
    attributes inside a device.

    :param device: Tango device name in a/b/c form
    :type device: str
    :param num_of_threads: Number of applications to launch. Default value is 5.
    :type num_of_threads: int
    """

    attrs = Device(device).get_attribute_list()
    my_process = ["taurus", "--polling-period=1000", "form"]
    for a in attrs:
        my_process.append(f"{device}/{a}")

    forms_ = []
    for i in range(num_of_threads):
        forms_.append(Popen(my_process))
        time.sleep(0.5)

if __name__ == "__main__":

    # Change Device name by your LLRF Tango device name:
    device = "sys/tg_test/1"

    # Launch Taurus Forms:
    main(device)